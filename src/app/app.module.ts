import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
// import { MapComponent } from './core/modules/map/map.component';
// import { ListComponent } from './core/modules/list/list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule,routingComponents} from './app-routing.module';
import { AgmCoreModule } from '@agm/core';
import { AgmOverlays } from "agm-overlays"
import {MaterialModule} from './material-module';
import { DataService } from './data.service';
import { HttpClientModule } from '@angular/common/http';
import { RatingModule } from 'ng-starrating';

@NgModule({
  declarations: [
    AppComponent,

    routingComponents
    // MapComponent,
    // ListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    AgmOverlays,
    RatingModule,
    AppRoutingModule,
    //routingComponents,
   AgmCoreModule.forRoot(
    {
      apiKey: 'AIzaSyBVj41fTYfeDrxPrLVtpWtmh7Pm-KSOLK8'
    }
   )
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }

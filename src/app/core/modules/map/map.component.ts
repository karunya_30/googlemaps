import { Component, ViewChild, ElementRef, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { DataService } from 'src/app/data.service';
//import { StarRatingComponent } from 'ng-starrating/components/star-rating/star-rating.component';
declare var google: any;
@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit {
  name = "Angular 5";
  public placeList;
  public bhel: Object = { lat: 15.91290, lng: 79.73999 };
  public map: any;
  public data;
  public isDisplay: boolean = false;
  public marker: any;
  public zoom: number;
  public selectedData: any;
  public clickedPlace;
  public clickedAddress;
  public reviews;
  public readonly: boolean;
  public buttonValue: string;
  public picture: any;
  public timings: any;
  public number: any;
  @ViewChild("map", { static: true }) mapRef: ElementRef;
  constructor(private service: DataService) {
  }
  ngOnInit(): void {
    this.getPlace();
  }
  getPlace(): void {
    this.service.getData().subscribe(data => {
      this.placeList = data;
    });
  }
  ngAfterViewInit() {
    this.map = new google.maps.Map(this.mapRef.nativeElement, {
      zoom: 1,
      center: this.bhel
    });
    this.marker = new google.maps.Marker({
      position: this.bhel,
      map: this.map
    });

  }
  updateMap(lats: number, lon: number, button) :void{
    this.buttonValue = button.name;
    this.marker.setMap(null);
    this.zoom = this.map.getZoom();
    this.marker = new google.maps.Marker({
      position: { lat: lats, lng: lon },
      map: this.map
    });

  }
  onRate($event: { oldValue: number, newValue: number, starRating: StarRatingComponent }) {
  }
  public closeButton() {
    this.isDisplay = false
  }
  public getDetails(data): void {
    this.isDisplay = true;
    this.selectedData = data;
    console.log(this.selectedData.name)
    this.clickedPlace = this.selectedData.name;
    this.clickedAddress = this.selectedData.address;
    this.reviews = this.selectedData.details;
    this.timings = this.selectedData.operating_hours;
    this.number = this.selectedData.number;
    this.picture = this.selectedData.logo
    console.log(this.reviews)
  }
}

import { ListComponent } from './core/modules/list/list.component';
import { MapComponent } from './core/modules/map/map.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  { path: '', redirectTo: 'place', pathMatch: 'full' },
  {
    path: 'place',
    component: MapComponent
  }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [MapComponent,ListComponent]

import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { throwError as observableThrowError } from 'rxjs';
import { catchError } from 'rxjs/operators'
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { PanelList } from 'src/assets/data';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  public url: string = '/assets/data/data.json';

  constructor(private http: HttpClient) { }

  getData(): Observable<PanelList[]> {
    return this.http.get<PanelList[]>(this.url)
      .pipe(
        catchError(this.errorHandler)
      );
  }
  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "Server Error");
  }
  getDetails() {
    throw new Error("Method not implemented.");
  }
}

export interface PanelList {
  id:number;
  name: string;
  lat: number;
  lng: number;
  logo:string;
  address:string;
  value:number;
  details:[{
    name:string;
    date:string;
    rating:number;
    comments:string;
  }
  ]
}
